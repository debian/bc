From: Ryan Kavanagh <rak@debian.org>
Date: Mon, 4 Mar 2024 13:29:04 -0500
Subject: Read ~/.dcrc at startup of dc

Origin: vendor
Bug-Debian: http://bugs.debian.org/582137
Bug-Ubuntu: https://bugs.launchpad.net/debian/+source/bc/+bug/918836
Forwarded: bug-bc@gnu.org, 2022-08-23
Reviewed-by: Ryan Kavanagh <rak@debian.org>
Last-Update: 2013-06-02

Patch taken from old version of dc by Paul Dwerryhouse
Was initially applied by Francois Marier <francois@debian.org> in the NMU
1.06.94-3.1 to close http://bugs.debian.org/472250 .
Then it disappeared along the way, until users asked for it again.
Jan Braun <janbraun@gmx.net> updated it to read .drcr before argument
processing so that
    $ dc
    $ dc -
    $ dc -f -
all have the same behaviour
---
 dc/dc.c  | 27 +++++++++++++++++++++++++++
 doc/dc.1 |  8 ++++++++
 2 files changed, 35 insertions(+)

diff --git a/dc/dc.c b/dc/dc.c
index 36c1825..b78f9ee 100644
--- a/dc/dc.c
+++ b/dc/dc.c
@@ -43,6 +43,8 @@
 # include <sys/stat.h>
 #endif
 #include <getopt.h>
+#include <pwd.h>
+#include <unistd.h>
 #include "dc.h"
 #include "dc-proto.h"
 #include "number.h"
@@ -247,6 +249,29 @@ flush_okay DC_DECLVOID()
 	return r;
 }
 
+static void
+try_rcfile(void)
+{
+    char *homedir;
+    struct passwd *pw;
+    char *rcfile;
+    FILE *input;
+
+    homedir=getenv("HOME");
+    if (!homedir)
+    {
+	pw=getpwuid(getuid());
+	homedir=pw->pw_dir;
+    }
+    rcfile=malloc(strlen(homedir)+8);
+    sprintf(rcfile, "%s/.dcrc", homedir);
+    if (!(input=fopen(rcfile, "r")))
+	return;
+    if (dc_evalfile(input))
+	exit(EXIT_FAILURE);
+    fclose(input);
+}
+
 
 int
 main DC_DECLARG((argc, argv))
@@ -269,6 +294,8 @@ main DC_DECLARG((argc, argv))
 	dc_register_init();
 	dc_array_init();
 
+	try_rcfile();
+
 	while ((c = getopt_long(argc, argv, "hVe:f:", long_opts, (int *)0)) != EOF) {
 		switch (c) {
 		case 'e':
diff --git a/doc/dc.1 b/doc/dc.1
index 7a79ad4..02c4abd 100644
--- a/doc/dc.1
+++ b/doc/dc.1
@@ -514,6 +514,14 @@ Thus \fB1 0:a 0Sa 2 0:a La 0;ap\fP will print 1,
 because the 2 was stored in an instance of 0:a that
 was later popped.
 .SH
+FILES
+.TP 15
+~/.dcrc
+The commands in this file will be executed when
+.I dc
+is first run.
+.PP
+.SH
 BUGS
 .PP
 Email bug reports to
